﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public string sceneToLoad = "Level1";

    //public Slider progressBar;

    private AsyncOperation loadingOperation;
    //private bool IsSceneLoading = false;

    public void OnPlayClicked()
    {
        //SceneManager.LoadScene("Level1");
        loadingOperation = SceneManager.LoadSceneAsync(sceneToLoad);
    }

    /*private void Update()
    {
        if (loadingOperation != null)
        {
            progressBar.value = Mathf.Clamp01(loadingOperation.progress / 0.9f);
        }
    }*/

    public void OnSettingsClicked()
    {
        // TODO: Add settings menu
    }

    public void OnCreditsClicked()
    {
        // TODO: Add credits for the assets
    }

    public void OnAboutClicked()
    {
        // TODO: Implement an About (me) View 
    }

    public void OnInstructionsClicked()
    {
        // TODO: Create a hot to play view
    }

    public void OnQuitClicked()
    {
        Application.Quit();
    }
}
