﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float tilt;
    public float fireRate;

    public GameObject shot;
    public GameObject explosionPlayerVFX;
    public Transform shotGeneratePosition;
    public AudioSource playerShooting;

    private Rigidbody _rigidbody;
    private GameController _gameController;

    private float _nextFire;

    void Start()
    {
        _rigidbody = gameObject.GetComponent<Rigidbody>();
        _gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    public void FixedUpdate()
    {
        MovePlayer();
        RotatePlayer();
    }

    private void MovePlayer()
    {
        float horAxis = Input.GetAxis("Horizontal");
        float verAxis = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(horAxis, 0.0f, verAxis);
        _rigidbody.velocity = movement * speed;
        _rigidbody.position = new Vector3(_rigidbody.position.x, 0.0f, _rigidbody.position.z);
    }

    private void RotatePlayer()
    {
        _rigidbody.rotation = Quaternion.Euler(0.0f, 0.0f, _rigidbody.velocity.x * -tilt);
    }

    public void Update()
    {
        if ( (Input.GetButton("Shot")) && Time.time > _nextFire)
        {
            PlayerShooting();
            _nextFire = Time.time + fireRate;
            Instantiate(shot, shotGeneratePosition.position, shotGeneratePosition.rotation);
        }
    }

    private void PlayerShooting()
    {
        playerShooting.Play();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Contains("Asteroid"))
        {
            Instantiate(explosionPlayerVFX, transform.position, transform.rotation);
            _gameController.PlayerCollidedWithAsteroid();
        }
    }
}
