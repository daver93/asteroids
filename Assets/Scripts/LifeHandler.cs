﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class LifeHandler : MonoBehaviour
{
    public Image life1, life2, life3;
    private List<Image> lives;

    private void Start()
    {
        InitializeLives();
    }

    private void InitializeLives()
    {
        lives = new List<Image>();

        lives.Add(life1);
        lives.Add(life2);
        lives.Add(life3);
    }

    public void PlayerLostOneLife()
    {
        lives[lives.Count - 1].enabled = false;
        lives.RemoveAt(lives.Count-1);
    }

    public int GetNumberOfLivesLeft()
    {
        return lives.Count;
    }
}
