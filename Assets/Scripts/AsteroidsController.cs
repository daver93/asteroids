﻿using UnityEngine;

public class AsteroidsController : MonoBehaviour
{
    public float asteroidSpeed;

    void Start()
    {
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        rigidbody.angularVelocity = Random.insideUnitSphere * 8;
        rigidbody.velocity = transform.forward * asteroidSpeed;
    }
}
