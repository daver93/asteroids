﻿using UnityEngine;

public class RepositionController : MonoBehaviour
{
    private GameController _gameController;

    public float _upperScreenLimit = -9;
    public float _downScreenLimit = 9;

    public float _leftScreenLimit = 17;
    public float _rightScreenLimit = -20;

    private Vector3 _position;

    private void Start()
    {
        _gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    private void FixedUpdate()
    {
        _position = gameObject.transform.position;

        if (IsObjectReachedBottomOfScreen())
        {
            PutObjectOnTopOfScreen();
        }
        else if (IsObjectReachedTopOfScreen())
        {
            PutObjectOnBottomOfScreen();
        }
        else if (IsObjectReachedLeftScreenLimit())
        {
            PutObjectRightOfScreen();
        }
        else if (IsObjectReachedRightScreenLimit()){
            PutObjectLeftOfScreen();
        }
    }

    private bool IsObjectReachedBottomOfScreen()
    {
        return _position.z > _downScreenLimit;
    }

    private bool IsObjectReachedTopOfScreen()
    {
        return _position.z < _upperScreenLimit;
    }

    private bool IsObjectReachedLeftScreenLimit()
    {
        return _position.x > _leftScreenLimit;
    }

    private bool IsObjectReachedRightScreenLimit()
    {
        return _position.x < _rightScreenLimit;
    }

    private void PutObjectOnTopOfScreen()
    {
        gameObject.transform.position = new Vector3(_position.x, _position.y, _gameController.spawnValues.z);
    }

    private void PutObjectOnBottomOfScreen()
    {
        gameObject.transform.position = new Vector3(_position.x, _position.y, _downScreenLimit);
    }

    private void PutObjectLeftOfScreen()
    {
        gameObject.transform.position = new Vector3(_leftScreenLimit, _position.y, _position.z);
    }

    private void PutObjectRightOfScreen()
    {
        gameObject.transform.position = new Vector3(_rightScreenLimit, _position.y, _position.z);
    }
}
