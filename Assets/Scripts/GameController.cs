﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameObject largeAsteroid;
    public GameObject mediumAsteroid;
    public GameObject smallAsteroid;
    
    public Vector3 spawnValues;

    public Text updateScore;
    public Text gameOver;
    public Text restart;

    public AudioSource asteroidExplosionAudio;
    public AudioSource playerExplosionAudio;

    public int _score;
    public float waitTimeBeforeStartSpawningAsteroids;
    public float waitTimeBetweenSpawningAsteroids;

    public int numberOfAsteroidsToSpawn;

    private LifeHandler _lifeHandler;

    private bool _canRestart;
    private bool _processOfSpawningStarted;
    private int _countAsteroidsOnScene = 0;

    private const int SUBPARTS_NUMBER_TO_SPLIT_ASTEROID = 2;

    void Start()
    {
        _canRestart = false;
        _processOfSpawningStarted = true;

        _lifeHandler = gameObject.GetComponent<LifeHandler>();

        waitTimeBetweenSpawningAsteroids = 1.5f;
        waitTimeBeforeStartSpawningAsteroids = 2f;

        UpdateScore();
        StartCoroutine(SpawnAsteroid());
    }

    private void FixedUpdate()
    {
        // Space is clear --> Spawn new asteroids
        if (_countAsteroidsOnScene == 0 && !_processOfSpawningStarted)
        {
            StartCoroutine(SpawnAsteroid());
        }
    }

    IEnumerator SpawnAsteroid()
    {
        _processOfSpawningStarted = true;

        yield return new WaitForSeconds(waitTimeBeforeStartSpawningAsteroids);

        for (int i = 0; i < numberOfAsteroidsToSpawn; i++)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
            Quaternion spawnRotation = Quaternion.identity;
            Instantiate(largeAsteroid, spawnPosition, spawnRotation);
            IncreaseAsteroidsCounter();
            yield return new WaitForSeconds(waitTimeBetweenSpawningAsteroids);
        }

        _processOfSpawningStarted = false;
    }

    public void IncreaseAsteroidsCounter()
    {
        _countAsteroidsOnScene++;
    }

    public void UpdateScore()
    {
        updateScore.text = "Score: " + _score;
    }
    
    public void InstantiateAsteroid(string destroyedAsteroidTag, Transform destroyedAsteroidTransform)
    {
        GameObject _asteroidToInstantiate = null;

        if (destroyedAsteroidTag == "LargeAsteroid")
        {
            _asteroidToInstantiate = mediumAsteroid;
        }
        else if (destroyedAsteroidTag == "MediumAsteroid")
        {
            _asteroidToInstantiate = smallAsteroid;
        }

        if (_asteroidToInstantiate != null)
        {
            for (int i = 0; i < SUBPARTS_NUMBER_TO_SPLIT_ASTEROID; i++)
            {
                Instantiate(_asteroidToInstantiate, destroyedAsteroidTransform.position, destroyedAsteroidTransform.rotation);
                IncreaseAsteroidsCounter();
            }
        }
    }

    public void AsteroidDestruction()
    {
        DecreaseAsteroidsCounter();
        asteroidExplosionAudio.Play();
    }

    private void DecreaseAsteroidsCounter()
    {
        _countAsteroidsOnScene--;
    }

    public void PlayerCollidedWithAsteroid()
    {
        if (_lifeHandler.GetNumberOfLivesLeft() > 1)
        {
            _lifeHandler.PlayerLostOneLife();
        }
        else
        {
            PlayerDestruction();
            GameOver();
        }
    }

    public void PlayerDestruction()
    {
        playerExplosionAudio.Play();
    }

    private void GameOver()
    {
        gameOver.gameObject.SetActive(true);
        restart.gameObject.SetActive(true);
        _canRestart = true;
        Time.timeScale = 0.0f;
    }

    private void OnGUI()
    {
        if (_canRestart && Input.GetButton("Restart"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            Time.timeScale = 1.0f;
        }
    }
}
