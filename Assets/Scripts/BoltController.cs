﻿using UnityEngine;

public class BoltController : MonoBehaviour
{
    public float boltSpeed;
    public float travelDistanceBeforeDestroy;

    private Vector3 _initialPosition;
    private float _travelledDistance;

    private void Start()
    {
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = transform.up * -boltSpeed;

        _initialPosition = gameObject.transform.position;

        if (travelDistanceBeforeDestroy <= 0)
        {
            travelDistanceBeforeDestroy = 1500;
        }
    }

    private void Update()
    {
        _travelledDistance += Vector3.Distance(_initialPosition, transform.position);
        if (_travelledDistance >= travelDistanceBeforeDestroy)
        {
            Destroy(gameObject);
        }
    }
}
