﻿using UnityEngine;

public class DestroyAsteroid : MonoBehaviour
{
    public GameObject explosionAsteroidVFX;

    private GameController _gameController;

    void Start()
    {
        _gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bolt")
        {
            DestroyBolt(other.gameObject);
            IncreaseScore();

            _gameController.InstantiateAsteroid(gameObject.tag, gameObject.transform);
            DestroyAsteroidProcess();
        }
    }

    private void DestroyBolt(GameObject bolt)
    {
        Destroy(bolt);
    }

    private void IncreaseScore()
    {
        if (gameObject.tag == "LargeAsteroid")
        {
            _gameController._score++;
        }
        else if (gameObject.tag == "MediumAsteroid")
        {
            _gameController._score += 2;
        }
        else if (gameObject.tag == "SmallAsteroid")
        {
            _gameController._score += 5;
        }

        _gameController.UpdateScore();
    }

    private void DestroyAsteroidProcess()
    {
        Instantiate(explosionAsteroidVFX, transform.position, transform.rotation);
        _gameController.AsteroidDestruction();
        Destroy(gameObject);
    }
}
